echo "Using the new Test.groovy file..."

def sbtHome = tool 'sbtLatest'
env.sbt = "${sbtHome}/bin/sbt"

sh "${sbt} test"
