echo "Using the new Build.groovy file..."

def sbtHome = tool 'sbtLatest'
env.sbt = "${sbtHome}/bin/sbt"

sh "${sbt} assembly"
