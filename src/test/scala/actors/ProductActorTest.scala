package actors

import actors.ActorData.{ ProductResponse, QueueResponse }
import akka.actor.{ ActorSystem, PoisonPill }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit, TestProbe }
import org.scalatest.{ BeforeAndAfterEach, WordSpecLike }

class ProductActorTest extends TestKit(ActorSystem("ProductActorTest")) with WordSpecLike with BeforeAndAfterEach with ImplicitSender {

  var productActor: TestActorRef[ProductActor] = _

  override def beforeEach(): Unit = {
    productActor = TestActorRef(new ProductActor)
  }
  "Product actor" should {
    "get products via queue" in {
      val prb = TestProbe()
      productActor ! QueueResponse(prb.ref, List("128128123"))
      prb.expectMsg(ProductResponse(List(Map("128128123" -> List("ELEC", "EV", "GAS")))))
    }
  }
  override def afterEach(): Unit = productActor ! PoisonPill

}
