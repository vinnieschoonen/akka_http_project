package actors

import actors.ActorData.{ OrderResponse, QueueResponse }
import akka.actor.{ ActorSystem, PoisonPill }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit, TestProbe }
import org.scalatest.{ BeforeAndAfterEach, WordSpecLike }

class OrderActorTest extends TestKit(ActorSystem("ProductActorTest")) with WordSpecLike with BeforeAndAfterEach with ImplicitSender {

  var orderActor: TestActorRef[OrderActor] = _

  override def beforeEach(): Unit = {
    orderActor = TestActorRef(new OrderActor)
  }
  "Order actor" should {
    "get products via queue" in {
      val prb = TestProbe()
      orderActor ! QueueResponse(prb.ref, List("128128123"))
      prb.expectMsgType[OrderResponse]
    }
  }
  override def afterEach(): Unit = orderActor ! PoisonPill
}
