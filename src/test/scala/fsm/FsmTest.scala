package fsm

import akka.actor.FSM.StateTimeout
import akka.actor.{ ActorRef, ActorSystem, PoisonPill }
import akka.testkit.{ TestActors, TestFSMRef }
import fsm.FsmData._
import org.scalatest.{ BeforeAndAfterEach, FlatSpec, FunSuite, Matchers }

class FsmTest extends FlatSpec with Matchers with BeforeAndAfterEach {
  implicit val system = ActorSystem()

  var fsm: TestFSMRef[FsmData.State, FsmData.Data, Fsm] = _
  val orderEcho: ActorRef = system.actorOf(TestActors.echoActorProps)

  override def beforeEach(): Unit = {
    fsm = TestFSMRef(new Fsm)
  }

  it should "start waiting and uninitialized" in {
    fsm.stateName shouldBe Waiting
    fsm.stateData shouldBe Uninitialized
  }

  it should "transition not to active for one after a queue message if target is not set" in {
    val msg = queueItem
    fsm ! msg
    fsm.stateName shouldBe Waiting
  }

  it should "transition to active for one after a queue message if target is set" in {
    fsm ! SetTarget(orderEcho)
    val msg = queueItem
    fsm ! msg
    fsm.stateName shouldBe Active
  }

  it should "transition to waiting when queue is flushed" in {
    fsm ! SetTarget(orderEcho)
    val msg = queueItem
    fsm ! msg
    fsm.stateName shouldBe Active
    fsm ! Flush
    fsm.stateName shouldBe Waiting
  }

  it should "transition to waiting when queue timeout is reached" in {
    fsm ! SetTarget(orderEcho)
    val msg = queueItem
    fsm ! msg
    fsm.stateName shouldBe Active
    fsm ! StateTimeout
    fsm.stateName shouldBe Waiting
  }

  override def afterEach(): Unit = fsm ! PoisonPill

  val queueItem = QueueItem(List("123456789"))
}