package server

import actors.ActorData._
import akka.actor.Actor
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.TestActorRef
import fsm.FsmData.{ Flush, QueueItem, QueueSize, SetTarget }
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ MustMatchers, WordSpec }

class RoutesTest extends WordSpec with MustMatchers with ScalaFutures with ScalatestRouteTest
  with Routes {
  lazy val routes = productRoutes(productActorRef, productFsmActorRef) ~ orderRoutes(orderActorRef, orderFsmActorRef) ~ defaultRoutes
  "ProductRoutes" should {
    "get products" in {
      Get("/product?q=125125125") ~> routes ~> check {
        status must be(OK)
        contentType must be(ContentTypes.`application/json`)
      }
    }
  }
  "OrderRoutes" should {
    "get orders" in {
      Get("/order?q=125125125") ~> routes ~> check {
        status must be(OK)
        contentType must be(ContentTypes.`application/json`)
      }
    }
  }
  "DefaultRoutes" should {
    "get index" in {
      Get("/index") ~> routes ~> check {
        status must be(OK)
        contentType must be(ContentTypes.`text/html(UTF-8)`)
      }
    }
  }

  val productActorRef = TestActorRef(new Actor {
    def receive = {
      case GetProducts(_) ⇒ ProductResponse(List(Map("128128121" -> List("ELEC"))))
    }
  })

  val orderActorRef = TestActorRef(new Actor {
    def receive = {
      case GetOrders(_) ⇒ OrderResponse(List(Map("128128121" -> "ELEC")))
    }
  })

  val productFsmActorRef = TestActorRef(new Actor {
    def receive = {
      case QueueSize ⇒ sender ! QueueSizeResponse(1)
      case Flush ⇒
      case QueueItem(_) => sender ! ProductResponse(List(Map("128128121" -> List("ELEC"))))
      case SetTarget(_) ⇒
    }
  })

  val orderFsmActorRef = TestActorRef(new Actor {
    def receive = {
      case QueueSize ⇒ sender ! QueueSizeResponse(1)
      case QueueItem(_) => sender ! OrderResponse(List(Map("128128121" -> "ELEC")))
      case SetTarget ⇒
    }
  })
}