package server

import org.scalatest.{ MustMatchers, WordSpecLike }
import actors.Proxy.toOrders

class ProxyTest extends WordSpecLike with MustMatchers {
  "Proxy" should {
    "forward order requests and return list of responses" in {
      val resp = toOrders(List(orderRequest, orderRequest))
      resp.length must be(2)
    }
  }
  val orderRequest: List[String] = List("123456789", "123456788")
}
