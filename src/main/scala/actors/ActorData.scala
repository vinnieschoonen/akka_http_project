package actors

import akka.actor.ActorRef

object ActorData {
  sealed trait CustomerData
  case class GetOrders(ids: List[String]) extends CustomerData
  case class GetProducts(ids: List[String]) extends CustomerData
  case class StreamElement(snd: ActorRef, ids: List[String]) extends CustomerData

  sealed trait CustomerResponses
  case class OrderResponse(orders: List[Map[String, String]]) extends CustomerResponses
  case class ProductResponse(products: List[Map[String, List[String]]]) extends CustomerResponses

  case class QueueResponse(snd: ActorRef, ids: List[String])
  case class QueueSizeResponse(size: Int)
}
