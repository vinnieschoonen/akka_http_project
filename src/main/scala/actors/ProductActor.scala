package actors

import actors.ActorData.{ GetProducts, ProductResponse, QueueResponse }
import akka.actor.{ Actor, ActorLogging, Props }
import scalaz.Scalaz._
import scalaz._
import server.QuickstartServer._

import scala.language.{ higherKinds, implicitConversions, postfixOps }
import scala.util.Random

object ProductActor {
  def props: Props = Props[ProductActor]
}
class ProductActor extends Actor with ActorLogging {
  override def receive = {
    case GetProducts(ids) =>
      val snd = sender()
      snd ! ProductResponse(getProducts(ids))
    case QueueResponse(snd, ids) =>
      snd ! ProductResponse(getProducts(ids))
  }

  private def getProducts(ids: List[String]): List[Map[String, List[String]]] = {
    ids.foldLeft(List[Map[String, List[String]]]())((acc, element) => acc :+ Map(element -> getProductsList(toAmount(element))))
  }

  private def getRndProduct: String = {
    products(Random.nextInt(products.size))
  }

  private def toAmount(id: String): Int = {
    parseInt(id.reverse.head.toString) match {
      case Success(i) => i
    }
  }

  private def toRandProdList(l: Int): List[String] = {
    val r = 1 to l - products.size
    val rs = r.toList
    rs.foldLeft(List[String]())((acc, _) => acc :+ getRndProduct)
  }

  private def getProductsList(s: Int): List[String] = {
    val r = Random.nextInt(products.size)
    s match {
      case 1 => List(products(r))
      case 2 => products.filterNot(p => p == products(r))
      case 3 => products
      case _ => products ::: toRandProdList(s)
    }
  }
}

