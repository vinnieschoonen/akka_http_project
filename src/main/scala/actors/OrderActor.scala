package actors

import actors.ActorData._
import akka.actor.{ Actor, ActorLogging, Props }
import server.QuickstartServer._

import scala.util.Random

object OrderActor {
  def props: Props = Props[OrderActor]
}

class OrderActor extends Actor with ActorLogging {

  override def receive: Receive = {
    case GetOrders(ids) =>
      val snd = sender()
      snd ! OrderResponse(getOrders(ids))
    case QueueResponse(snd, ids) =>
      snd ! OrderResponse(getOrders(ids))
    case elements: Vector[StreamElement] =>
      elements.foreach { e =>
        e.snd ! OrderResponse(getOrders(e.ids))
      }
    case m @ _ => println(s"Unknown message for orderactor: $m")
  }

  private def getOrders(ids: List[String]): List[Map[String, String]] = {
    ids.foldLeft(List[Map[String, String]]())((acc, element) => acc :+ Map(element -> getRndOrder))
  }

  private def getRndOrder: String = {
    orders(Random.nextInt(orders.size))
  }
}
