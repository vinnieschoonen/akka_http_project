package actors

import actors.ActorData.OrderResponse
import server.QuickstartServer.orders

import scala.util.Random

object Proxy {
  def toOrders(ids: List[List[String]]): List[OrderResponse] = {
    ids.map(id => OrderResponse(getOrders(id)))
  }

  private def getOrders(ids: List[String]): List[Map[String, String]] = {
    ids.foldLeft(List[Map[String, String]]())((acc, element) => acc :+ Map(element -> getRndOrder))
  }

  private def getRndOrder: String = {
    orders(Random.nextInt(orders.size))
  }
}

