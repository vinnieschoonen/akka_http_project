package actors

import actors.ActorData.{ GetOrders, StreamElement }
import akka.actor.{ Actor, ActorLogging, Props }
import akka.stream.scaladsl.{ Sink, Source, SourceQueueWithComplete }
import akka.stream.{ ActorMaterializer, OverflowStrategy }
import server.QuickstartServer.orderActor

import scala.concurrent.duration._

object OrderStreamActor {
  def props: Props = Props[OrderStreamActor]
}

class OrderStreamActor extends Actor with ActorLogging {
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val queue: SourceQueueWithComplete[StreamElement] = Source
    .queue[StreamElement](0, OverflowStrategy.backpressure)
    .groupedWithin(5, 10 second)
    .to(Sink.actorRef(orderActor, "Completed!"))
    .run()

  override def receive: Receive = {
    case GetOrders(ids) =>
      val snd = sender()
      queue.offer(StreamElement(snd, ids))
  }
}
