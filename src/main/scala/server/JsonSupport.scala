package server

import actors.ActorData.{ OrderResponse, ProductResponse }

//#json-support
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport {
  // import the default encoders for primitive types (Int, String, Lists etc)
  import DefaultJsonProtocol._

  implicit val productFormat = jsonFormat1(ProductResponse)
  implicit val orderFormat = jsonFormat1(OrderResponse)
}
