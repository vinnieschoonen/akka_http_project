package server

import actors.ActorData.{ GetOrders, OrderResponse, ProductResponse, QueueSizeResponse }
import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import akka.http.scaladsl.model.ContentTypes.`text/html(UTF-8)`
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.StatusCodes.{ BadRequest, NotFound }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.{ Route, RouteConcatenation }
import akka.pattern.ask
import akka.util.Timeout
import fsm.FsmData.{ Flush, QueueItem, QueueSize, SetTarget }
import server.QuickstartServer.maxQueueSize

import scala.concurrent.duration._
import scala.util.{ Failure => ScalaFailure, Success => ScalaSuccess }

trait Routes extends RouteConcatenation with JsonSupport {
  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[Routes])

  // Required by the `ask` (?) method below
  implicit lazy val timeout = Timeout(10.seconds) // usually we'd obtain the timeout from the system's configuration

  def productRoutes(productAct: ActorRef, productFsm: ActorRef): Route =
    path("product") {
      get {
        parameters('q.as[String]) { q ⇒
          val qS = toIds(q)
          productFsm ! SetTarget(productAct)
          onComplete(
            productFsm ? QueueSize) {
              case ScalaSuccess(QueueSizeResponse(r)) =>
                if (r == maxQueueSize) {
                  println("[ProductFSM] Flush the queue!")
                  productFsm ! Flush
                  onComplete(
                    productFsm ? QueueItem(qS)) {
                      case ScalaSuccess(ProductResponse(rsp)) =>
                        complete(ProductResponse(rsp))
                      case ScalaSuccess(_) =>
                        complete(NotFound)
                      case ScalaFailure(e) =>
                        println(s"[ProductFSM] Failure back in the route  because $e")
                        complete(BadRequest)
                    }
                } else {
                  onComplete(
                    productFsm ? QueueItem(qS)) {
                      case ScalaSuccess(ProductResponse(rsp)) =>
                        complete(ProductResponse(rsp))
                      case ScalaSuccess(_) =>
                        complete(NotFound)
                      case ScalaFailure(e) =>
                        println(s"[ProductFSM] Failure back in the route  because $e")
                        complete(BadRequest)
                    }
                }
              case ScalaSuccess(_) =>
                complete(NotFound)
              case ScalaFailure(e) =>
                println(s"[ProductFSM] Failure back in the route  because $e")
                complete(BadRequest)
            }
        }
      }
    }

  def orderRoutes(orderAct: ActorRef, orderFsm: ActorRef): Route =
    path("order") {
      get {
        parameters('q.as[String]) { q ⇒
          val qS = toIds(q)
          orderFsm ! SetTarget(orderAct)
          onComplete(
            orderFsm ? QueueSize) {
              case ScalaSuccess(QueueSizeResponse(r)) =>
                if (r == maxQueueSize) {
                  println("[OrderFSM] Flush the queue!")
                  orderFsm ! Flush
                  onComplete(
                    orderFsm ? QueueItem(qS)) {
                      case ScalaSuccess(OrderResponse(rsp)) =>
                        complete(OrderResponse(rsp))
                      case ScalaSuccess(_) =>
                        complete(NotFound)
                      case ScalaFailure(e) =>
                        println(s"[OrderFSM] Failure back in the route  because $e")
                        complete(BadRequest)
                    }
                } else {
                  onComplete(
                    orderFsm ? QueueItem(qS)) {
                      case ScalaSuccess(OrderResponse(rsp)) =>
                        complete(OrderResponse(rsp))
                      case ScalaSuccess(_) =>
                        complete(NotFound)
                      case ScalaFailure(e) =>
                        println(s"[OrderFSM] Failure back in the route  because $e")
                        complete(BadRequest)
                    }
                }
              case ScalaSuccess(_) =>
                complete(NotFound)
              case ScalaFailure(e) =>
                println(s"[OrderFSM] Failure back in the route  because $e")
                complete(BadRequest)
            }
        }
      }
    }

  def orderStreamRoutes(orderStreamAct: ActorRef): Route =
    path("orderStream") {
      get {
        parameters('q.as[String]) { q ⇒
          val qS = toIds(q)
          onComplete(
            orderStreamAct ? GetOrders(qS)) {
              case ScalaSuccess(OrderResponse(rsp)) =>
                complete(OrderResponse(rsp))
              case ScalaSuccess(_) =>
                complete(NotFound)
              case ScalaFailure(e) =>
                println(s"[OrderStreamActor] Failure back in the route  because $e")
                complete(BadRequest)
            }
        }
      }
    }

  def defaultRoutes: Route =
    path("index") {
      get {
        complete(HttpEntity(`text/html(UTF-8)`, "<h1>Say hello to akka-http.</h1>"))
      }
    }

  private def toIds(q: String): List[String] = {
    q.split(",").toSet.toList.filter(_.length == 9).sorted
  }

}
