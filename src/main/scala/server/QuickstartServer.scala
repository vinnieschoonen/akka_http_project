package server

import actors.{ OrderActor, OrderStreamActor, ProductActor }
import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import fsm.Fsm

import scala.concurrent.Await
import scala.concurrent.duration.Duration

import com.typesafe.config.{ Config, ConfigFactory }

object QuickstartServer extends App with Routes {

  implicit val system: ActorSystem = ActorSystem("helloAkkaHttpServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val config = ConfigFactory.load()

  val host = config.getString("http.host") // Gets the host and a port from the configuration
  val port = config.getInt("http.port")

  lazy val orders: List[String] = List("DELIVERED", "NEW", "ORDERING", "SETUP")
  lazy val products: List[String] = List("ELEC", "EV", "GAS")
  lazy val maxQueueSize: Int = 5
  lazy val maxQueueTimeout: Int = 5

  lazy val productActor: ActorRef = system.actorOf(ProductActor.props, "productActor")
  lazy val orderActor: ActorRef = system.actorOf(OrderActor.props, "orderActor")
  val orderStreamActor: ActorRef = system.actorOf(OrderStreamActor.props, "orderStreamActor")
  val productFsm: ActorRef = system.actorOf(Fsm.props, "productFsm")
  val orderFsm: ActorRef = system.actorOf(Fsm.props, "orderFsm")

  def routes: Route = productRoutes(productActor, productFsm) ~ orderRoutes(orderActor, orderFsm) ~ orderStreamRoutes(orderStreamActor) ~ defaultRoutes

  //#http-server
  Http().bindAndHandle(routes, host, port)

  println(s"Server online at http://localhost:5000/")

  Await.result(system.whenTerminated, Duration.Inf)
}
