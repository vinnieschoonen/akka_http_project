package fsm

import actors.ActorData.{ QueueResponse, QueueSizeResponse }
import akka.actor.{ FSM, Props }
import fsm.FsmData._
import server.QuickstartServer._

import scala.concurrent.duration._
import scala.language.postfixOps

object Fsm {
  def props: Props = Props[Fsm]
}

class Fsm extends FSM[State, Data] {
  startWith(Waiting, Uninitialized)

  when(Waiting) {
    case Event(SetTarget(ref), Uninitialized) ⇒
      println(s"[FSM] Set target actor ref to $ref")
      stay using QueueToProcess(ref, Vector.empty)
  }

  onTransition {
    case Active -> Waiting ⇒
      stateData match {
        case QueueToProcess(ref, queue) ⇒ {
          println("[FSM] Go to idle and flush queue")
          queue.foreach(i ⇒
            ref ! QueueResponse(i.snd, i.q.obj))
        }
        case _ ⇒ // do nothing
      }
  }

  when(Active, stateTimeout = maxQueueTimeout second) {
    case Event(Flush | StateTimeout, t: QueueToProcess) ⇒
      println("[FSM] State active receive flush/timeout")
      goto(Waiting) using t.copy(queue = Vector.empty)
  }

  whenUnhandled {
    case Event(QueueItem(obj), t @ QueueToProcess(_, v)) ⇒
      println(s"[FSM] Received event, go to active and add item ${v.size + 1} to queue.")
      val snd = sender()
      goto(Active) using t.copy(queue = v :+ QueueRef(snd, QueueItem(obj)))
    case Event(QueueSize, QueueToProcess(_, v)) ⇒
      sender() ! QueueSizeResponse(v.size)
      stay
    case Event(e, s) ⇒
      log.warning("[FSM] Received unhandled request {} in state {}/{}", e, stateName, s)
      stay
  }

  initialize()
  println("[FSM] Initialized")
}
