package fsm

import akka.actor.ActorRef
import scala.language.{ higherKinds, implicitConversions, postfixOps }

object FsmData {
  // received events
  final case class SetTarget(ref: ActorRef)
  final case class QueueItem(obj: List[String])
  final case class QueueRef(snd: ActorRef, q: QueueItem)
  case object QueueSize
  case object Flush

  // states
  sealed trait State
  case object Waiting extends State
  case object Active extends State

  sealed trait Data
  case object Uninitialized extends Data
  final case class QueueToProcess(target: ActorRef, queue: Seq[QueueRef]) extends Data
}
