# akka_http_project

### Run project or unit tests
 sbt run/test

#### Default akka http url
 http://localhost:5000/index

#### Example get products
 http://localhost:5000/product?q=121121121,121121122,121121123,121121124,121121125,121121126,121121127,121121128,121121129
 
 #### Example get orders 
  http://localhost:5000/order?q=121121121,121121122,121121123,121121124,121121125,121121126,121121127,121121128,121121129

#### Example get orders stream
 http://localhost:5000/orderStream?q=121121121,121121122,121121123,121121124,121121125,121121126,121121127,121121128,121121129

